package com.davi.minhasvagas;

/**
 * Created by Lapada on 19-Feb-16.
 */
public class SiteDeEmprego
{
    private String nomeSite;
    private String descricao;
    private String url;
    private long id;



    public SiteDeEmprego(String nome, String desc, String url)
    {
        this.nomeSite = nome;
         this.descricao = desc;
         this.url = url;


    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomeSite() {
        return nomeSite;
    }
    public String getDescricao() {
        return descricao;
    }

    public String getUrl() {
        return url;
    }

    public int getImagemLogo(int position){
        switch (position)
        {
            case 0:
                //Retornar imagem do /quero workar
                return R.drawable.malaxxhdpi;
               // break;
            case 1:
                //retornar imagem do sfiec
                return R.drawable.malaxxhdpi;
               // break;
            case 2:
                return R.drawable.malaxxhdpi;
           // break;
            default:
                return R.drawable.malaxxhdpi;

        }
    }

}
