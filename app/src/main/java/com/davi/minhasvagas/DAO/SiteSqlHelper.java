package com.davi.minhasvagas.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Carlos David on 22-Mar-16.
 */
public class SiteSqlHelper extends SQLiteOpenHelper{
    private static final String NOME_BANCO = "dbMeuEmprego";
    private static final int VERSAO_BANCO = 1;
     static final String TABELA_SITES = "sites";
    public static final String COLUNA_ID = "_id";
    public static final String COLUNA_NOME_SITE = "nomeSite";
    public static final String COLUNA_DESC_SITE = "descSite";
    public static final String COLUNA_URL_SITE = "urlSite";

    public SiteSqlHelper(Context context)
    {
        super(context,NOME_BANCO,null,VERSAO_BANCO);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL
                (
                        "CREATE TABLE "+TABELA_SITES+ " ("+
                        COLUNA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                        COLUNA_NOME_SITE+" TEXT NOT NULL,"+
                        COLUNA_DESC_SITE+" TEXT,"+
                        COLUNA_URL_SITE+" TEXT NOT NULL)"
                );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //PARA PRÓXIMAS VERSÕES
    }

}
