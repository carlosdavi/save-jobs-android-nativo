package com.davi.minhasvagas.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.davi.minhasvagas.SiteDeEmprego;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos David on 22-Mar-16.
 */
public class SiteRepositorio {
    private SiteSqlHelper siteSqlHelper;

    public SiteRepositorio(Context context)
    {
        siteSqlHelper = new SiteSqlHelper(context);
    }

    private long inserir(SiteDeEmprego siteDeEmprego)
    {
        SQLiteDatabase database = siteSqlHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(SiteSqlHelper.COLUNA_NOME_SITE,siteDeEmprego.getNomeSite());
        contentValues.put(SiteSqlHelper.COLUNA_DESC_SITE,siteDeEmprego.getDescricao());
        contentValues.put(SiteSqlHelper.COLUNA_URL_SITE,siteDeEmprego.getUrl());

        long id = database.insert(siteSqlHelper.TABELA_SITES,null,contentValues);

        if(id != -1)
        {
            siteDeEmprego.setId(id);
        }
        database.close();
        return id;
    }
    private int atualizar(SiteDeEmprego siteDeEmprego)
    {
        SQLiteDatabase database = siteSqlHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(SiteSqlHelper.COLUNA_NOME_SITE,siteDeEmprego.getNomeSite());
        contentValues.put(SiteSqlHelper.COLUNA_DESC_SITE,siteDeEmprego.getDescricao());
        contentValues.put(SiteSqlHelper.COLUNA_URL_SITE,siteDeEmprego.getUrl());

        int linhasAfetadas = database.update
                (
                        SiteSqlHelper.TABELA_SITES,
                        contentValues,
                        SiteSqlHelper.COLUNA_ID + " = ?",
                        new String[]{String.valueOf(siteDeEmprego.getId())}
                );
        database.close();
        return linhasAfetadas;
    }
    public void salvar(SiteDeEmprego siteDeEmprego)
    {
        if(siteDeEmprego.getId() == 0)
        {
            inserir(siteDeEmprego);
        }
        else
        {
            atualizar(siteDeEmprego);
        }
    }
    public int excluir(SiteDeEmprego siteDeEmprego)
    {
        SQLiteDatabase database = siteSqlHelper.getWritableDatabase();
        int linhasAfetadas = database.delete
                (
                        SiteSqlHelper.TABELA_SITES,
                        SiteSqlHelper.COLUNA_ID +" = ?",
                        new String[]{String.valueOf(siteDeEmprego.getId())}

                );
        database.close();
        return linhasAfetadas;
    }

    public List<SiteDeEmprego> buscaSiteDeemprego(String filtro)
    {
        SQLiteDatabase database = siteSqlHelper.getReadableDatabase();

        String sql = "SELECT * FROM "+ SiteSqlHelper.TABELA_SITES;
        String[] argumentos = null;
        if(filtro != null)
        {
            sql+=" WHERE "+SiteSqlHelper.COLUNA_NOME_SITE +" LIKE ?";
            argumentos = new String[]{filtro};
        }
        sql += " ORDER BY " + SiteSqlHelper.COLUNA_ID;
        Cursor cursor = database.rawQuery(sql,argumentos);

        List<SiteDeEmprego> listaSiteDeEmprego = new ArrayList<>();
        while(cursor.moveToNext())
        {
            long id = cursor.getLong(cursor.getColumnIndex(SiteSqlHelper.COLUNA_ID));
            String nomeSite  = cursor.getString(cursor.getColumnIndex(SiteSqlHelper.COLUNA_NOME_SITE));
            String urlSite = cursor.getString(cursor.getColumnIndex(SiteSqlHelper.COLUNA_URL_SITE));
            String descricaoSite = cursor.getString(cursor.getColumnIndex(SiteSqlHelper.COLUNA_DESC_SITE));
            SiteDeEmprego siteDeEmprego = new SiteDeEmprego(nomeSite,descricaoSite,urlSite);
            listaSiteDeEmprego.add(siteDeEmprego);
        }
        cursor.close();
        database.close();
        return listaSiteDeEmprego;
    }
}
