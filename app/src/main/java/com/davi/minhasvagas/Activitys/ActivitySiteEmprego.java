package com.davi.minhasvagas.Activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.davi.minhasvagas.R;

import java.util.ArrayList;
import java.util.List;

public class ActivitySiteEmprego extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final ProgressDialog progressDialog;


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_emprego);

        webView = (WebView)findViewById(R.id.webViewSites);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando...");

        WebSettings ws =  webView.getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setSupportZoom(true);
        TextView nomeSite = (TextView)findViewById(R.id.textoSite);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        nomeSite.setText(bundle.getString("nomeSite"));

        webView.loadUrl(bundle.getString("url"));
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                progressDialog.dismiss();
                return false;
            }
        });



        String appKey = "6049f38e111e50c560534f99554f4ab42dfc7651680f3444";
        //Appodeal.setBannerViewId(R.id.appodealBannerView);
        Appodeal.initialize(this, appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.MREC | Appodeal.REWARDED_VIDEO);
        // Appodeal.setTesting(true);
        Appodeal.show(this, Appodeal.INTERSTITIAL);
        Appodeal.show(this, Appodeal.BANNER_BOTTOM);

    }
    @Override
    public void onResume(){
        super.onResume();
        Appodeal.show(this, Appodeal.BANNER_BOTTOM);
    }

    private boolean gettingOut = false;
    @Override
    public void onBackPressed() {
        if (gettingOut) {
            super.onBackPressed();
            finish();
        } else {
            gettingOut = true;
            Appodeal.show(this, Appodeal.INTERSTITIAL);
            Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
                @Override
                public void onInterstitialLoaded(boolean b) {

                }

                @Override
                public void onInterstitialFailedToLoad() {

                }

                @Override
                public void onInterstitialShown() {

                }

                @Override
                public void onInterstitialClicked() {

                }

                @Override
                public void onInterstitialClosed() {
                    onBackPressed();
                }
            });
        }


    }
}
