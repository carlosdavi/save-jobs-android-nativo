package com.davi.minhasvagas.Activitys;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.davi.minhasvagas.AdaptadordeSites;
import com.davi.minhasvagas.DAO.SiteRepositorio;
import com.davi.minhasvagas.Fragments.CustomDialogFragment;
import com.davi.minhasvagas.R;
import com.davi.minhasvagas.SiteDeEmprego;

import java.util.List;

import static com.davi.minhasvagas.R.color.corToobal;

public class Activity_Principal extends AppCompatActivity implements View.OnClickListener,ListView.OnItemClickListener {
    ListView listView;
    ArrayAdapter arrayAdapter;
    List<SiteDeEmprego> listaSites;
    FloatingActionButton fab;
    SiteRepositorio siteRepositorio;
    AdaptadordeSites adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        setViews();
        setDados();



    }

    public void setViews()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);





        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(this);

        siteRepositorio = new SiteRepositorio(getBaseContext());


    }

    public void setDados() {
        listaSites = BuscarSites(null);
        if(listaSites.isEmpty())
        {
            Snackbar.make((View)findViewById(R.id.tv), "Você não tem nada cadastrado! =/ \n Clique em cadastrar e comece =))", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
        else if(listaSites == null)
        {
            Snackbar.make((View)findViewById(R.id.tv), "Você não tem nada BANCO! =/ \n Clique em cadastrar e comece =))", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        else
        {
            adapter = new AdaptadordeSites(this,listaSites);
            listView.setAdapter(adapter);
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab)
        {
            OpenDialogFragment(v);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(Activity_Principal.this, ActivitySiteEmprego.class);
        Bundle bundle = new Bundle();
        bundle.putString("nomeSite", listaSites.get(position).getNomeSite());
        bundle.putString("url", listaSites.get(position).getUrl());
        intent.putExtras(bundle);
        startActivity(intent, bundle);
    }

    public List<SiteDeEmprego> BuscarSites(String condicao) {
        if (condicao != null) {
            return siteRepositorio.buscaSiteDeemprego("%" + condicao + "%");
        } else {
            return siteRepositorio.buscaSiteDeemprego(condicao);
        }
    }

    public void OpenDialogFragment(View view)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        CustomDialogFragment customDialogFragment = new CustomDialogFragment();
        customDialogFragment.show(fragmentTransaction,"dialog");

    }
    public void CloseDialogFragment(View view)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        CustomDialogFragment customDialogFragment = (CustomDialogFragment)getSupportFragmentManager().findFragmentByTag("dialog");
        if(customDialogFragment != null)
        {
            customDialogFragment.dismiss();
            fragmentTransaction.remove(customDialogFragment);
        }


    }
    public void AddInfos(View view){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        CustomDialogFragment customDialogFragment = (CustomDialogFragment)getSupportFragmentManager().findFragmentByTag("dialog");


        EditText nomeSite = ((EditText)customDialogFragment.getView().findViewById(R.id.edtNomeSite));
        EditText siteDescricao = ((EditText)customDialogFragment.getView().findViewById(R.id.edtDescricao));
        EditText siteUrl = ((EditText)customDialogFragment.getView().findViewById(R.id.edtURL));
        SiteDeEmprego site = new SiteDeEmprego(nomeSite.getText().toString(),siteDescricao.getText().toString(),siteUrl.getText().toString());
        siteRepositorio.salvar(site);
        CloseDialogFragment(view);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onResume(){
        super.onResume();

    }

   private boolean gettingOut = false;
    @Override
    public void onBackPressed(){
        if(gettingOut){
            super.onBackPressed();
            finish();
        }
        else{
            gettingOut = true;
            Appodeal.show(this, Appodeal.INTERSTITIAL);
            Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
                @Override
                public void onInterstitialLoaded(boolean b) {

                }

                @Override
                public void onInterstitialFailedToLoad() {

                }

                @Override
                public void onInterstitialShown() {

                }

                @Override
                public void onInterstitialClicked() {

                }

                @Override
                public void onInterstitialClosed() {
                    onBackPressed();

                }
            });
        }




  }
}