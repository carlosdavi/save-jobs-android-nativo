package com.davi.minhasvagas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Lapada on 19-Feb-16.
 */
public class AdaptadordeSites extends BaseAdapter {
    List<SiteDeEmprego> lista;
    Context context;

    public AdaptadordeSites(Context context, List<SiteDeEmprego> lista){
        this.context = context;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        SiteDeEmprego site = lista.get(position);
        View layout;

        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = inflater.inflate(R.layout.layout_lista_sites, null);
        }
        else
        {
            layout = view;
        }

        ImageView imagemsite =(ImageView) layout.findViewById(R.id.logoEmpresa);
        TextView nomeEmpresa = (TextView)layout.findViewById(R.id.nomeEmpresa);

        TextView descricao = (TextView)layout.findViewById(R.id.descricaoEmpresa);
        descricao.setText(site.getDescricao());

        imagemsite.setImageResource(site.getImagemLogo(position));
        nomeEmpresa.setText(site.getNomeSite());

            return layout;
    }
}
